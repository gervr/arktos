def add_database(name: str) -> None:
    print(f"Database '{name}' was added.")
