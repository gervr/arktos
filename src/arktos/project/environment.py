from pathlib import Path
from typing import Optional

CONFIG_DIRECTORY = ".arktos"


def find_environment() -> Optional[Path]:
    path = Path(".").absolute()
    while path.name != "":
        project_path = path.joinpath(CONFIG_DIRECTORY)
        if project_path.is_dir():
            return project_path
        path = path.parent
    return None
