from arktos.cli import cli


def main():
    cli()  # pylint: disable=no-value-for-parameter


if __name__ == "__main__":
    main()
