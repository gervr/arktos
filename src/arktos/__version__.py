import datetime

__author__ = "Ger van Rossum"
__author_email__ = "gvr@users.noreply.github.com"
__copyright__ = f"Copyright (c) {datetime.datetime.now().year} {__author__}"
__description__ = "Arktos test project."
__license__ = "MIT"
__url__ = "https://gitlab.com/gervr/arktos"
__version__ = "0.1.0"
