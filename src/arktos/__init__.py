"""
Arktos - a Python test project

Metadata is imported from a __version__.py to allow setup.py to reuse the data
without having to import anything.
"""
from .__version__ import __author__  # noqa: F401
from .__version__ import __author_email__  # noqa: F401
from .__version__ import __copyright__  # noqa: F401
from .__version__ import __description__  # noqa: F401
from .__version__ import __license__  # noqa: F401
from .__version__ import __url__  # noqa: F401
from .__version__ import __version__  # noqa: F401
