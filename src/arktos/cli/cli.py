from typing import Dict, Any

import click
from click.core import Context

from arktos import __version__ as version
from .commands import add
from .commands import create
from .commands import remove
from .commands import show
from .commands import update

CONTEXT_SETTINGS: Dict[str, Any] = dict()


@click.group(context_settings=CONTEXT_SETTINGS)
@click.option("-v", "--verbose", default=False, is_flag=True, help="Be talkative.")
@click.version_option(version=version, message="%(prog)s %(version)s")
@click.pass_context
def cli(ctx: Context, verbose: bool):
    ctx.ensure_object(dict)
    ctx.obj["verbose"] = verbose
    if verbose:
        click.echo("Verbosity on...")


cli.add_command(create)

cli.add_command(add)
cli.add_command(remove)
cli.add_command(show)
cli.add_command(update)
