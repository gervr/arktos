import functools

import click

import arktos
from arktos.project.environment import find_environment


def only_inside_project(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        root = find_environment()
        if find_environment() is None:
            click.echo(f"fatal: not inside an {arktos.__name__} project directory")
            click.core.fast_exit(1)
        else:
            func(root, *args, **kwargs)

    return wrapper
