import click
from click.core import Context


@click.command()
@click.pass_context
@click.argument("name")
def create(ctx: Context, name: str):
    """Create a new project."""
    display_name = click.style(name, fg="red", bold=True)
    click.echo(f"create project {display_name}....")
    ctx.exit(0)
