from pathlib import Path

import click
from click.core import Context

import arktos.cli.click_extensions as click_extensions
from arktos.actions.database_actions import add_database


@click.group("add")
@click.pass_context
def add(ctx: Context):
    verbose = ctx.obj["verbose"]
    if verbose:
        click.echo("Adding something...")


@add.command("database")
@click.argument("name", required=False)
@click.pass_context
@click_extensions.only_inside_project
def database(project: Path, ctx: Context, name: str):
    verbose = ctx.obj["verbose"]
    if verbose:
        click.echo("Adding database...")
    name = name or click.prompt("Database name")

    try:
        add_database(name)
    except Exception:  # noqa: pylint: disable=broad-except
        click.echo("something went wrong...")
        ctx.exit(1)

    click.echo(f">>> inside project path {project}")
    ctx.exit(0)


@add.command("bla")
@click.pass_context
@click_extensions.only_inside_project
def bla(root: Path, ctx: Context):
    verbose = ctx.obj["verbose"]
    if verbose:
        click.echo("BLA...")
    click.echo(f">>> inside project path {root}")
    ctx.exit(0)
