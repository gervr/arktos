import click
from click.core import Context


@click.command()
@click.pass_context
def show(ctx: Context):
    click.echo("show something....")
    ctx.exit(0)
