import click
from click.core import Context


@click.command()
@click.pass_context
def remove(ctx: Context):
    click.echo("remove something....")
    ctx.exit(0)
