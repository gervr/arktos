import click
from click.core import Context


@click.command()
@click.pass_context
def update(ctx: Context):
    click.echo("update something....")
    ctx.exit(0)
