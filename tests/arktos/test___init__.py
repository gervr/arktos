from unittest import TestCase

import arktos


class InitSpec(TestCase):
    """
    Test the arktos.__init__.py
    """

    def test_author(self):
        self.assertIsNotNone(arktos.__author__)

    def test_author_email(self):
        self.assertRegex(arktos.__author_email__, "\\w*@\\w*")

    def test_copyright(self):
        self.assertRegex(arktos.__copyright__, "Copyright \\(c\\) \\d{4} ")

    def test_description(self):
        self.assertIsNotNone(arktos.__description__)

    def test_license(self):
        self.assertIsNotNone(arktos.__license__)

    def test_name(self):
        self.assertEqual(arktos.__name__, "arktos")

    def test_url(self):
        self.assertEqual(arktos.__url__, "https://gitlab.com/gervr/arktos")

    def test_version(self):
        self.assertIsNotNone(arktos.__version__)
