import os

from setuptools import setup

name = "arktos"

here = os.path.abspath(os.path.dirname(__file__))
module_path = os.path.join(here, "src", name)


def read(path, file):
    with open(os.path.join(path, file), "r") as f:
        contents = f.read()
    return contents


info = {}
exec(read(module_path, "__version__.py"), info)
readme = read(here, "README.md")

setup(
    name=name,
    version=info["__version__"],
    description=info["__description__"],
    long_description=readme,
    long_description_content_type="text/markdown",
    author=info["__author__"],
    author_email=info["__author_email__"],
    url=info["__url__"],
    license=info["__license__"],
    package_dir={"": "src"},
    packages=[name],
    install_requires=["click"],
    platforms=["any"],
    entry_points={"console_scripts": ["arktos=arktos.__main__:main"]},
)
